﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Occurences
/// </summary>

// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
public class Webinar
{
    public string uuid { get; set; }
    public DateTime start_time { get; set; }
    public string occurrence_id { get; set; }
}

public class WebinarRoot
{
    public List<Webinar> webinars { get; set; }
}
public class Occurrence
{
    public string occurrence_id { get; set; }
    public DateTime start_time { get; set; }
    public int duration { get; set; }
    public string status { get; set; }
}

public class Settings
{
    public bool host_video { get; set; }
    public bool panelists_video { get; set; }
    public int approval_type { get; set; }
    public int registration_type { get; set; }
    public string audio { get; set; }
    public string auto_recording { get; set; }
    public bool enforce_login { get; set; }
    public string enforce_login_domains { get; set; }
    public string alternative_hosts { get; set; }
    public bool close_registration { get; set; }
    public bool show_share_button { get; set; }
    public bool allow_multiple_devices { get; set; }
    public bool practice_session { get; set; }
    public bool hd_video { get; set; }
    public bool question_answer { get; set; }
    public bool registrants_confirmation_email { get; set; }
    public bool on_demand { get; set; }
    public bool request_permission_to_unmute_participants { get; set; }
    public string contact_name { get; set; }
    public string contact_email { get; set; }
    public int registrants_restrict_number { get; set; }
    public bool registrants_email_notification { get; set; }
    public bool post_webinar_survey { get; set; }
    public string survey_url { get; set; }
    public bool meeting_authentication { get; set; }
    public List<object> additional_data_center_regions { get; set; }
}

public class Recurrence
{
    public int type { get; set; }
    public int repeat_interval { get; set; }
    public string weekly_days { get; set; }
    public int end_times { get; set; }
}

public class OccurrenceRoot
{
    public string uuid { get; set; }
    public long id { get; set; }
    public string host_id { get; set; }
    public string host_email { get; set; }
    public string topic { get; set; }
    public long type { get; set; }
    public int duration { get; set; }
    public string timezone { get; set; }
    public string agenda { get; set; }
    public DateTime created_at { get; set; }
    public string start_url { get; set; }
    public string join_url { get; set; }
    public string registration_url { get; set; }
    public List<Occurrence> occurrences { get; set; }
    public Settings settings { get; set; }
    public Recurrence recurrence { get; set; }
}