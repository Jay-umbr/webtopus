﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Registrant
/// </summary>
public class CustomQuestion
{
    public string title { get; set; }
    public string value { get; set; }
}

public class Registrant
{
    public string id { get; set; }
    public string email { get; set; }
    public string country { get; set; }
    public string phone { get; set; }
    public List<CustomQuestion> custom_questions { get; set; }
}

public class RegistrantRoot
{
    public int page_size { get; set; }
    public int total_records { get; set; }
    public string next_page_token { get; set; }
    public List<Registrant> registrants { get; set; }
}
