﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Participant
/// </summary>
 public class Root
{
    public int page_count { get; set; }
public int page_size { get; set; }
public int total_records { get; set; }
public string next_page_token { get; set; }
public List<Participant> participants { get; set; }
}
public class Participant
{
    public string id { get; set; }
    public string user_id { get; set; }
    public string name { get; set; }
    public string user_email { get; set; }
    public DateTime join_time { get; set; }
    public DateTime leave_time { get; set; }
    public int duration { get; set; }
    public string attentiveness_score { get; set; }
    public string phone { get; set; }
    public string country { get; set; }
    public List<CustomQuestion> custom_questions { get; set; }
}