﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Text;
using System.Web;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Models.PublishedContent;
using System.Linq;
using System.Web.UI.WebControls;
using System.Net;
using System.Web.Script.Serialization;

namespace SheetsApi
{

    class SheetsProgram
    {
        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.com-dotnet-quickstart.json
        static string[] Scopes = { SheetsService.Scope.Spreadsheets, "https://www.googleapis.com/auth/cloud-platform" };
        static string ApplicationName = "Zoom API service";

        public static Tuple<int, string> CallingList(string sheetId, IEnumerable<IPublishedElement> attendees, string gridName)
        {
            #region AppendRows
            UserCredential credential;
            byte[] byteArray = Encoding.UTF8.GetBytes(File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/credentials.json")).ReadToEnd());
            //byte[] byteArray = Encoding.ASCII.GetBytes(contents);
            MemoryStream stream = new MemoryStream(byteArray);
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(stream).Secrets,
                Scopes,
                "user",
                CancellationToken.None).Result;
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            var appendValueRange = new ValueRange();
            var oblist= new List<object>() { " " };
            appendValueRange.Values = new List<IList<object>> { oblist };
            IEnumerable<IPublishedElement> approvedAttendees = attendees.Where(x => x.GetProperty("canWeEmailYou").GetValue().ToString() == "Yes");
            if(approvedAttendees.Count() == 0)
            {
                Debug.WriteLine("0 people to update - aborting this method");
                return new Tuple<int, string>(2, "0 people to update - aborting this method");
            }
            foreach (var attendee in attendees)
            {
                var fullname = attendee.GetProperty("user_name").GetValue().ToString();
                string firstname;
                string lastname;
                if(fullname.Contains(" "))
                {
                    var spaceIndex = fullname.IndexOf(" ");
                    firstname = fullname.Substring(0, spaceIndex);
                    lastname = fullname.Substring(spaceIndex + 1);
                }
                else
                {
                    firstname = fullname;
                    lastname = "";
                }
                var upobj = new List<object>() { firstname, lastname, attendee.GetProperty("user_email").GetValue().ToString(), " " , attendee.GetProperty("doYouUseUmbraco").GetValue().ToString()};
                appendValueRange.Values.Add(upobj);
            }
            appendValueRange.Values.Remove(appendValueRange.Values.First());
            string range3 = gridName + "!A1";
            SpreadsheetsResource.ValuesResource.AppendRequest append = service.Spreadsheets.Values.Append(appendValueRange, sheetId, range3);
            append.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            try
            {
                append.Execute();
                return new Tuple<int, string>(1, "");
            }
            catch (Exception ex)
                {
                Debug.WriteLine("AppendRows failed!");
                Debug.WriteLine(ex.Message);
                return new Tuple<int, string>(1, ex.Message);
            }
            #endregion
        }

        public static List<int> GetCounter(string todaysdate)
        {
            UserCredential credential;

            // The file token.json stores the user's access and refresh tokens, and is created
            // automatically when the authorization flow completes for the first time.
            // convert string to stream
            var stream2 = new Installed();
            stream2.client_id = "1051881371711-m0q78bgbvs134bakn8520r00hg4hsv9d.apps.googleusercontent.com";
            stream2.project_id = "zoomstats-1605271087811";
            stream2.auth_uri = "https://accounts.google.com/o/oauth2/auth";
            stream2.token_uri = "https://oauth2.googleapis.com/token";
            stream2.auth_provider_x509_cert_url = "https://www.googleapis.com/oauth2/v1/certs";
            stream2.client_secret = "H9rLi1Sq3ZwCEoyPmlqqOWaD";
            stream2.javascript_origins = new List<string>();
            stream2.javascript_origins.Add("https://webtopus.azurewebsites.net");
            stream2.redirect_uris = new List<string>();
            stream2.redirect_uris.Add("https://webtopus.azurewebsites.net");

            var stream3 = new RootInstalled();
            stream3.installed = stream2;
            var json = new JavaScriptSerializer().Serialize(stream3);
            //byte[] byteArray = Encoding.UTF8.GetBytes(File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/credentials.json")).ReadToEnd());
            //byte[] byteArray = Encoding.ASCII.GetBytes(json.ToString());
            //Debug.WriteLine(json.ToString());
            byte[] byteArray = Encoding.UTF8.GetBytes(File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/credentials.json")).ReadToEnd());
            MemoryStream stream = new MemoryStream(byteArray);
            //Debug.WriteLine(System.Web.HttpContext.Current.Server.MapPath("~/credentials.json"));
            //Debug.WriteLine(stream.ToString());
            //string credPath = "token.json";

            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(stream).Secrets,
                Scopes,
                "user",
                CancellationToken.None).Result;
            //new FileDataStore(credPath, true)).Result;
            //Debug.WriteLine("Credential file saved to: " + credPath);
            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            //String spreadsheetId = "1YkQ1jN7VBTlzpkK5KPtTyDfU0uMNFT5pQbfe-WhyVL4";
            String spreadsheetId = "1T2cPs9d6i5tAerI-2NkMFS-n7fki6cGD1W4v-qTsKl8";
            int numberTotal = 0;
            int numberToday = 0;
            int plannedToday = 0;
            string range = todaysdate;
            //DateTime.Now.ToString("d.M.yyyy")
            Debug.WriteLine("date: " + range);
            SpreadsheetsResource.ValuesResource.GetRequest request =
                    service.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                Debug.WriteLine("A, B");
                Debug.WriteLine(values.Count());
                foreach (var row in values)
                {
                    // Print columns A and E, which correspond to indices 0 and 4.
                    //Debug.WriteLine("{0}, {1}", row[0], row[1]);

                    Debug.WriteLine("+++ " + row[1].ToString());

                    if (row[1].ToString().Contains("done") || row[1].ToString().Contains("Done"))
                    {
                        numberToday++;
                    }
                }
                plannedToday = values.Count() -1;
                
                Debug.WriteLine("Planned today: " +plannedToday);
                Debug.WriteLine("Done today: " + numberToday);
            }
            else
            {
                Debug.WriteLine("No data found.");
            }
            List<int> results = new List<int>();
            results.Add(numberToday);
            results.Add(plannedToday);
            return results;
        }

            public static Tuple<int, string> Main(string sheetId, string date, IEnumerable<IPublishedElement> attendees, string nodeName)
        {
            UserCredential credential;

            // The file token.json stores the user's access and refresh tokens, and is created
            // automatically when the authorization flow completes for the first time.
            // convert string to stream
            Debug.WriteLine("Main date is: " + date + ", or " + date);
            var stream2 = new Installed();
            stream2.client_id = "1051881371711-m0q78bgbvs134bakn8520r00hg4hsv9d.apps.googleusercontent.com";
            stream2.project_id = "zoomstats-1605271087811";
            stream2.auth_uri = "https://accounts.google.com/o/oauth2/auth";
            stream2.token_uri = "https://oauth2.googleapis.com/token";
            stream2.auth_provider_x509_cert_url = "https://www.googleapis.com/oauth2/v1/certs";
            stream2.client_secret = "H9rLi1Sq3ZwCEoyPmlqqOWaD";
            stream2.javascript_origins = new List<string>();
            stream2.javascript_origins.Add("https://webtopus.azurewebsites.net");
            stream2.redirect_uris = new List<string>();
            stream2.redirect_uris.Add("https://webtopus.azurewebsites.net");

            var stream3 = new RootInstalled();
            stream3.installed = stream2;
            var json = new JavaScriptSerializer().Serialize(stream3);
            //byte[] byteArray = Encoding.UTF8.GetBytes(File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/credentials.json")).ReadToEnd());
            //byte[] byteArray = Encoding.ASCII.GetBytes(json.ToString());
            //Debug.WriteLine(json.ToString());
            byte[] byteArray = Encoding.UTF8.GetBytes(File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/credentials.json")).ReadToEnd());
            MemoryStream stream = new MemoryStream(byteArray);
            //Debug.WriteLine(System.Web.HttpContext.Current.Server.MapPath("~/credentials.json"));
            //Debug.WriteLine(stream.ToString());
            //string credPath = "token.json";
           
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(stream).Secrets,
                Scopes,
                "user",
                CancellationToken.None).Result;
                    //new FileDataStore(credPath, true)).Result;
                //Debug.WriteLine("Credential file saved to: " + credPath);
            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            //String spreadsheetId = "1YkQ1jN7VBTlzpkK5KPtTyDfU0uMNFT5pQbfe-WhyVL4";
             String spreadsheetId = sheetId;
            string reportdate = date;
            bool weGood = true;
            if (reportdate == "January 01, 0001")
            {
                //reportdate = nodeName.Substring(nodeName.Length - 10);
                weGood = false;
            }
            #region AddSheetRequest
            var addSheetRequest = new AddSheetRequest();
            addSheetRequest.Properties = new SheetProperties();
            addSheetRequest.Properties.Title = reportdate;
            addSheetRequest.Properties.Index = 0;
            #endregion

            #region batchUpdate

            ValueRange valueRange = new ValueRange();
            valueRange.MajorDimension = "ROWS";//"ROWS";//COLUMNS

            var oblist = new List<object>() { "Name", "Email", "Join time", "Leave time", "Duration (minutes)", "Phone nr", "Do you use Umbraco", "Can we email you" };
            var oblistEmpty = new List<object>() { "" };
            var oblistEnd  = new List<object>() { " ", " ", " ", " ", " ", " ", " ", " ", "This report was generated automatically" };
            valueRange.Values = new List<IList<object>> { oblist };
            valueRange.Values.Add(oblistEmpty);
            foreach (var attendee in attendees)
            {
                int duration = Convert.ToInt32(attendee.GetProperty("duration").GetValue().ToString()) / 60;
                var oblistNew = new List<object>() { attendee.GetProperty("user_name").GetValue().ToString(),
                    attendee.GetProperty("user_email").GetValue().ToString(),
                    attendee.GetProperty("join_time").GetValue().ToString(),
                    attendee.GetProperty("leave_time").GetValue().ToString(),
                    duration,
                    attendee.GetProperty("phone").GetValue().ToString(),
                    attendee.GetProperty("doYouUseUmbraco").GetValue().ToString(),
                    attendee.GetProperty("canWeEmailYou").GetValue().ToString(),
                };
                valueRange.Values.Add(oblistNew);
            }
            valueRange.Values.Add(oblistEmpty);
            valueRange.Values.Add(oblistEnd);

            String range2 = reportdate + "!A1";
            SpreadsheetsResource.ValuesResource.UpdateRequest update = service.Spreadsheets.Values.Update(valueRange, spreadsheetId, range2);
            update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;

            try
            {
                BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
                batchUpdateSpreadsheetRequest.Requests = new List<Request>();
                batchUpdateSpreadsheetRequest.Requests.Add(new Request { AddSheet = addSheetRequest });
                var batchUpdateRequest = service.Spreadsheets.BatchUpdate(batchUpdateSpreadsheetRequest, spreadsheetId);
                batchUpdateRequest.Execute();
                UpdateValuesResponse resultUpdate = update.Execute();
                Debug.WriteLine("Report generated successfully!");
                if (weGood.Equals(false)) {
                    return new Tuple<int, string>(2, "Name set to DATETIME DEFAULT 1 JAN 0001");
                }
                else {
                    return new Tuple<int, string>(1, "");
                }
            }
            catch (Exception ex)
                {
                Debug.WriteLine("Report not generated!");
                Debug.WriteLine(ex.Message);
                return new Tuple<int, string>(0, ex.Message);
            }            

            #endregion
        }
    }
}