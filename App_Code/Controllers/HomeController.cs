﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedModels;
using System.Diagnostics;
using Umbraco.Core.Models.PublishedContent;
using System.Collections.Generic;
using System.Text;
using System;
using NPoco.Expressions;
using System.IdentityModel.Protocols.WSTrust;
using System.Threading.Tasks;
using Newtonsoft.Json;
//using MoreLinq; using Core instead
using SheetsApi;
using Umbraco.Core.Logging;
using System.IO;
using Umbraco.Core;
using System.Web;

namespace CS.Controllers {
    public class HomeController : SurfaceController
    {
        public HomeController()
        {
        }

        public ActionResult Counter()
        {
            string date = DateTime.Now.ToString("dd.MM.yyyy");
            List<int> results = SheetsProgram.GetCounter(date);
            // if (ViewBag.PreviousResults = results.FirstOrDefault()) { } else { ViewBag.PreviousResult = results.FirstOrDefault(); }

            Random r = new Random();
            int randomNumber = r.Next(1, 3);
            string imgSource = "";
            switch (randomNumber)
            {
                case 1:
                    imgSource = "/img/kim1.png";
                    break;
                case 2:
                    imgSource = "/img/kim2.png";
                    break;
                case 3:
                    imgSource = "/img/kim3.png";
                    break;
            }
                    ViewBag.PreviousResult = results.FirstOrDefault();
            ViewBag.CurrentResult = results.FirstOrDefault();
            ViewBag.PlannedResult = results.LastOrDefault();
            ViewBag.KimPic = imgSource;
            return View("~/Views/Counter.cshtml");
        }

        public ActionResult TransferDataToExcel(int pageId)
        {
            // Debug.WriteLine(pageId);
            Session["status"] = "";
            var targetNode = Umbraco.Content(pageId);
            var attendees = targetNode.Value<IEnumerable<IPublishedElement>>("attendeeList");
            string date = targetNode.Value<string>("date");
            string webinarType = targetNode.Parent.Name; //this can possibly be deleted
            string sheetId = targetNode.Parent.Value<string>("sheetId");
            //Debug.WriteLine(attendees.First().GetProperty("duration").Value());
            Debug.WriteLine("Preparing to push data to Excel sheet with id " + sheetId + " for " + webinarType + " webinar, for date " + date + " with " + attendees.Count() + " attendees");
            var outcome = SheetsProgram.Main(sheetId, date, attendees, targetNode.Name);
            if (outcome.Item1 == 1)
            {
                Session["status"] = "Transfer of data to Excel sheet - success! Do not forget to also transfer to webinar permissions. Target was: https://docs.google.com/spreadsheets/d/"+sheetId;
                var returnUrl = targetNode.Url();
                if (Url.IsLocalUrl(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return View("~/Views/ControlPanel.cshtml");
                }
            }
            else if (outcome.Item1 == 0)
            {
                Logging(outcome.Item2);
                return View("~/Views/Errors/genericExcuse.cshtml");
            }
            else if (outcome.Item1 == 2)
            {
                Logging(outcome.Item2);
                return View("~/Views/Errors/excuseWrongDate.cshtml");
            }
            else
            {
                return View("~/Views/ControlPanel.cshtml");
            }
        }

        public ActionResult ProcessUploadedFile(StreamReader report)
        {

            Debug.WriteLine("Report uploaded and processing started");

            Debug.WriteLine(report.ReadToEnd());
            return PartialView("~/Views/TestPartial.cshtml");
        }

        public ActionResult UploadResult(HttpPostedFileBase file)
        {
            //var ms = Services.MediaService;
            //Use the MediaService to create a new Media object (-1 is Id of root Media object, "File" is the MediaType)
            //var mediaFile = ms.CreateMedia("UploadTest", -1, "File");
            //set the umbracofile property with upload file
            //FileStream s = new FileStream(Request.QueryString["uploadFile"], FileMode.Open);
            //mediaFile.SetValue("umbracoFile", Path.GetFileName(Request.QueryString["uploadFile"]), s.ToString());
            ///Use the MediaService to Save the new Media object
            //ms.Save(mediaFile);

            // Verify that the user selected a file
            if (file != null)
            {
                // extract only the fielname
                var fileName = Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                Debug.WriteLine(fileName);
                Debug.WriteLine(path);
                //file.SaveAs(path);
            }
            // redirect back to the index action to show the form once again
            return View("~/Views/ControlPanel.cshtml");
        }

        public string Logging(string message)
        {
            Logger.Warn<HomeController>("Webinar tools: {message}", message);
            return $"{message}";
        }

        public ActionResult AddPeopleToExcel(int pageId) {
            Session["status"] = "";
            var targetNode = Umbraco.Content(pageId);
            var attendees = targetNode.Value<IEnumerable<IPublishedElement>>("attendeeList").Where(x=>x.Value<string>("canWeEmailYou") == "Yes");
            var homeNode = targetNode.Parent.Parent;
            Debug.WriteLine("HomeNode get? " + homeNode.Name);
            string gridName = targetNode.Parent.Value<string>("gridName");
            string statSheetId = homeNode.Value<string>("webinarPermissionsSheetId");
            Debug.WriteLine("Called AddPeopleToExcel");
            Debug.WriteLine("Spreadsheet ID for the stat is " + statSheetId);
            var outcome = SheetsProgram.CallingList(statSheetId, attendees, gridName);
            if (outcome.Item1 == 1) 
            {
                Session["status"] = "Transfer of 'Can we email you' people to permissions sheet - success! Target was: https://docs.google.com/spreadsheets/d/"+statSheetId;
                var returnUrl = targetNode.Url() + "?case=1"; ;
                if (Url.IsLocalUrl(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return View("~/Views/ControlPanel.cshtml");
                }
            }
            else if (outcome.Item1 == 2)
            {
                Logging(outcome.Item2);
                return View("~/Views/Errors/notEnoughAttendeesToAdd.cshtml");
            }
            else
            {
                Logging("Generic error");
                return View("~/Views/Errors/genericExcuse.cshtml");
            }
        }

        public async Task<ActionResult> GetWebinar(string target, string type)
        {
            Session["status"] = "";
            //target is webinar ID
            //targetFull is report/webinars/415152715/participants for example
            string fullTarget = "report/webinars/" + target + "/participants";
            var response = await ApiCaller.Main(fullTarget);
            Root report = JsonConvert.DeserializeObject<Root>(response);
            string latestOccurrence = await GetOccurrence(target, report.participants.FirstOrDefault().join_time.Date);
            Debug.WriteLine(latestOccurrence + "is the latest occurrence that was not deleted. Status: available");
            List<Registrant> registrants = await GetRegistrants(target, latestOccurrence);
            if (registrants.Count() == 0)
            {
                return View("~/Views/Errors/excuseZoomNoRegistrants.cshtml");
            }
            if (report.participants.Count() == 0)
            {
                return View("~/Views/Errors/excuseZoomNoParticipants.cshtml");
            }
            Debug.WriteLine("+++ Registrants before Umbraco purge: a total of " + registrants.Count());
            Debug.WriteLine("Latest webinar report start time: " + report.participants.FirstOrDefault().join_time.Date);
            report.participants.RemoveAll(u => u.user_email.Contains("@umbraco")); //we remove umbraco emails since they did not register and are not eligible for reports
            //registrants.RemoveAll(u => u.email.Contains("@umbraco")); //we remove umbraco emails since they did not register and are not eligible for reports
            Debug.WriteLine("+++ Registrants: a total of " + registrants.Count());
            foreach (var registrant in registrants)
            {
                Debug.WriteLine(registrant.email);
                Debug.WriteLine("Country:" +registrant.country);

                //Debug.WriteLine("+++ Registrants: " + registrant.email + " has id of " + registrant.id);
                //NOTE: id of registrant and participant is the same
            }
            foreach (var participant in report.participants)
            {
                Debug.WriteLine("%%% Participants: " + participant.user_email + " has id of " + participant.id);
                if(participant.id != "") { 
                participant.phone = registrants.Where(x => x.id == participant.id).FirstOrDefault().phone;
                    participant.country = registrants.Where(x => x.id == participant.id).FirstOrDefault().country;
                    participant.custom_questions = registrants.Where(x => x.id == participant.id).FirstOrDefault().custom_questions; //binding registrant value to participant
                }
                else
                {
                    participant.id = registrants.Where(x => x.email.ToLower() == participant.user_email.ToLower()).FirstOrDefault().id; //manually bind ID by using email
                    participant.phone = registrants.Where(x => x.id == participant.id).FirstOrDefault().phone;
                    participant.country = registrants.Where(x => x.id == participant.id).FirstOrDefault().country;
                    participant.custom_questions = registrants.Where(x => x.id == participant.id).FirstOrDefault().custom_questions; //binding registrant value to participant
                }
            }
            Debug.WriteLine("Shearing final list... count: " + report.participants.Count());
            var finalList = report.participants.DistinctBy(x => x.id).ToList();
            foreach (var item in finalList)
            {
                Debug.WriteLine("Actual attendees: " + item.user_email + " , who has phone nr " + item.phone + " and answered to the survey with " + item.custom_questions.FirstOrDefault().value + " " + item.custom_questions.Last().value);
                //NOTE: id of registrant and participant is the same
            }
            Debug.WriteLine("Actual attendees final count: " + finalList.Count());
            int result = GenerateNewNode(type, finalList, registrants.Count());
            if(result == 1) { return View("~/Views/ControlPanel.cshtml"); }
            else if (result == 2) { return View("~/Views/Errors/excuseZoomMethodNodeExists.cshtml"); }
            else { return View("~/Views/Errors/genericExcuseZoomMethod.cshtml"); }
        }



        public async Task<ActionResult> GetWebinarByOccurrence(string target, string uuid, string type, int pageId, string occurrence)
        {
            Session["substatus"] = "";
            //target is webinar ID
            //targetFull is report/webinars/415152715/participants for example
            string fullTarget = "report/webinars/" + uuid + "/participants";
            //https://api.zoom.us/v2/report/webinars/hHrhe3JhTqaGLoHXUmbfKA==/participants
            var response = await ApiCaller.Main(fullTarget);
            Root report = JsonConvert.DeserializeObject<Root>(response);

            List<Registrant> registrants = await GetRegistrants(target, occurrence);
            if (registrants.Count() == 0)
            {
                return View("~/Views/Errors/excuseZoomNoRegistrants.cshtml");
            }
            if (report.participants.Count() == 0)
            {
                return View("~/Views/Errors/excuseZoomNoParticipants.cshtml");
            }
            Debug.WriteLine("+++ Registrants before Umbraco purge: a total of " + registrants.Count());
            Debug.WriteLine("Latest webinar report start time: " + report.participants.FirstOrDefault().join_time.Date);
            report.participants.RemoveAll(u => u.user_email.Contains("@umbraco")); //we remove umbraco emails since they did not register and are not eligible for reports
            //registrants.RemoveAll(u => u.email.Contains("@umbraco")); //we remove umbraco emails since they did not register and are not eligible for reports
            Debug.WriteLine("+++ Registrants: a total of " + registrants.Count());
            foreach (var registrant in registrants)
            {
                Debug.WriteLine(registrant.email);
                Debug.WriteLine("Country:" + registrant.country);

                //Debug.WriteLine("+++ Registrants: " + registrant.email + " has id of " + registrant.id);
                //NOTE: id of registrant and participant is the same
            }
            foreach (var participant in report.participants)
            {
                Debug.WriteLine("%%% Participants: " + participant.user_email + " has id of " + participant.id);
                if (participant.id != "")
                {
                    participant.phone = registrants.Where(x => x.id == participant.id).FirstOrDefault().phone;
                    participant.country = registrants.Where(x => x.id == participant.id).FirstOrDefault().country;
                    participant.custom_questions = registrants.Where(x => x.id == participant.id).FirstOrDefault().custom_questions; //binding registrant value to participant
                }
                else
                {
                    participant.id = registrants.Where(x => x.email.ToLower() == participant.user_email.ToLower()).FirstOrDefault().id; //manually bind ID by using email
                    participant.phone = registrants.Where(x => x.id == participant.id).FirstOrDefault().phone;
                    participant.country = registrants.Where(x => x.id == participant.id).FirstOrDefault().country;
                    participant.custom_questions = registrants.Where(x => x.id == participant.id).FirstOrDefault().custom_questions; //binding registrant value to participant
                }
            }
            Debug.WriteLine("Shearing final list... count: " + report.participants.Count());
            var finalList = report.participants.DistinctBy(x => x.id).ToList();
            foreach (var item in finalList)
            {
                Debug.WriteLine("Actual attendees: " + item.user_email + " , who has phone nr " + item.phone + " and answered to the survey with " + item.custom_questions.FirstOrDefault().value + " " + item.custom_questions.Last().value);
                //NOTE: id of registrant and participant is the same
            }
            Debug.WriteLine("Actual attendees final count: " + finalList.Count());
            int result = GenerateNewNode(type, finalList, registrants.Count());
            if (result == 1) {
                Session["substatus"] = "Successfully generated a webinar report!";
                var targetNode = Umbraco.Content(pageId);
                var returnUrl = targetNode.Url();
                if (Url.IsLocalUrl(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return View("~/Views/ControlPanel.cshtml");
                }
        }
            else if (result == 2) {
                Session["substatus"] = "Last webinar report could not be generated.";
                return View("~/Views/Errors/excuseZoomMethodNodeExists.cshtml"); }
            else { return View("~/Views/Errors/genericExcuseZoomMethod.cshtml"); }
        }

        public async Task<ActionResult> GetAllOccurrences(string target, int pageId)
        {
            Session["substatus"] = "";
            //api.zoom.us/v2/webinars/812323648?show_previous_occurrences=true
            string fullTarget = "past_webinars/" + target + "/instances";
            //https://api.zoom.us/v2/past_webinars/812323648/instances
            var response = await ApiCaller.Main(fullTarget);
            Debug.WriteLine(response.ToString());
            WebinarRoot occurrences = JsonConvert.DeserializeObject<WebinarRoot>(response);
            if (occurrences.webinars.Count == 0)
            {
                return View("~/Views/Errors/genericExcuseZoomMethod.cshtml");
            }
            else
            {
                List<Tuple<string,Tuple<string, DateTime>>> occurrenceList = new List<Tuple<string,Tuple<string, DateTime>>> ();

                foreach (var item in occurrences.webinars)
                {
                    Debug.WriteLine("UUID: " + item.uuid + " on date " + item.start_time);
                    occurrenceList.Add(new Tuple<string, Tuple<string, DateTime>>(item.uuid, new Tuple<string, DateTime>(item.occurrence_id, item.start_time)));
                }
                //.ToString("MMMM dd, yyyy")
                occurrenceList.Sort((x, y) => y.Item2.Item2.CompareTo(x.Item2.Item2)); 
                TempData["occurrenceList"] = occurrenceList;

                var targetNode = Umbraco.Content(pageId);
                var returnUrl = targetNode.Url();
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return View("~/Views/ControlPanel.cshtml");
                    }
            }
        }

        public async Task<string> GetOccurrence(string target, DateTime date)
        {
            //api.zoom.us/v2/webinars/812323648?show_previous_occurrences=true
            Debug.WriteLine("GetOccurrence: looking for occurrence for webinar with id " + target + " with date " + date);
            string fullTarget = "webinars/" + target + "?show_previous_occurrences=true";
            var response = await ApiCaller.Main(fullTarget);
            Debug.WriteLine(response.ToString());
            OccurrenceRoot occurrences = JsonConvert.DeserializeObject<OccurrenceRoot>(response);
            var latest = occurrences.occurrences.Where(x => x.status == "available" && x.start_time.Date == date).Last(); //getting last because we get oldest webinars first
            Debug.WriteLine("Latest occurrence start time: " + latest.start_time.Date);
            return latest.occurrence_id;
        }

        public async Task<List<Registrant>> GetRegistrants(string webinarId, string webinarOccurrence)
        {
            //api.zoom.us/v2/webinars/812323648?show_previous_occurrences=true
            string fullTarget = "webinars/" + webinarId + "/registrants?occurrence_id=" + webinarOccurrence + "&page_size=100";
            var response = await ApiCaller.Main(fullTarget);
            List<Registrant> registrantsList = JsonConvert.DeserializeObject<RegistrantRoot>(response).registrants;
            return registrantsList;
        }

        public int GenerateNewNode(string webinarType, List<Participant> participants, int registrants) 
        {
            Debug.WriteLine("Looking to generate a new node for " + webinarType + " webinar");
            var ContentService = Services.ContentService;
            string date = DateTime.Now.ToString("MMMM dd, yyyy");
            if (participants.Count() > 0)
            {
                date = participants.FirstOrDefault().join_time.Date.ToString("MMMM dd, yyyy");
            }
            var guid = Umbraco.ContentAtRoot().FirstOrDefault().ChildrenOfType("webinarType").Where(x => x.Name == webinarType).FirstOrDefault().Key;
            var count = Umbraco.ContentAtRoot().FirstOrDefault().ChildrenOfType("webinarType").Where(x => x.Name == webinarType).FirstOrDefault().Children.Count();
            string nodeName = webinarType + " webinar on " + date;
            var nodeExists = Umbraco.ContentAtRoot().FirstOrDefault().DescendantsOrSelf("webinar").Any(n => n.Name == nodeName);
            if(nodeExists == true)
            {
                return 2;
            }
            var attendees = new List<Dictionary<string, string>>();
            IContent request = ContentService.Create(nodeName, guid, "webinar", -1);
            request.SetValue("date", date);
            request.SetValue("signups", registrants);
            foreach (var person in participants)
            {
                attendees.Add(new Dictionary<string, string>() {
           // {"key",request.Key.ToString()},
           // {"name", person.id},
            {"ncContentTypeAlias","attendee"},
            {"user_name", person.name},
            {"user_email",person.user_email},
            {"join_time",person.join_time.ToString()},
            {"leave_time",person.leave_time.ToString()},
            {"duration",person.duration.ToString()},
            {"phone",person.phone.ToString()},
            {"country",person.country.ToString()},
            {"doYouUseUmbraco",person.custom_questions.First().value.ToString()},
            {"canWeEmailYou",person.custom_questions.Last().value.ToString()}
            });
            }
            //attendeeList
            request.SetValue("attendeeList", JsonConvert.SerializeObject(attendees));
            ContentService.SaveAndPublish(request);
            return 1;
            //return Redirect(Request.UrlReferrer.ToString()); //redirect to the same page so the navigation refreshes
        }
    }
}