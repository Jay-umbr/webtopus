﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using Umbraco.Web.WebApi;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Threading.Tasks;

/// <summary>
/// Summary description for ApiCaller
/// </summary>
public class ApiCaller
{
    public ApiCaller()
    {
        //  
        // TODO: Add constructor logic here
        //
    }


   public static async Task<string> Main(string endpoint)
    {
        using (var client = new HttpClient())
        {
            client.BaseAddress = new Uri("https://api.zoom.us/v2/");   
            client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var authorization = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkRGLXFjX2RUUlpTUzlDaEhESmxQcEEiLCJleHAiOjE2NzA4NDM1MjAsImlhdCI6MTYwNDMxMzYxMX0.0pdb36zGnjZC7c42DqfH8At-_pKQ2cIfNijWDViQXKk";
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorization);
            //GET Method  
            // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6IkRGLXFjX2RUUlpTUzlDaEhESmxQcEEiLCJleHAiOjE2MDE5ODQ2OTAsImlhdCI6MTYwMTk3OTI4OX0.cnmfwAbA2EfebmsEuwepkPVyzHJok6vYzi1ZOJeEmfg
            HttpResponseMessage response = await client.GetAsync(endpoint);
            Debug.WriteLine(client.BaseAddress + endpoint);
            if (response.IsSuccessStatusCode)
            {
                Debug.WriteLine("API Response Positive!");
                string responseText;
                responseText = await response.Content.ReadAsStringAsync();
                //Debug.WriteLine(responseText);
                return responseText;
            }
            else
            {
                Debug.WriteLine("Internal server Error");
                Debug.WriteLine(response.StatusCode);
                return null;
            }
        }
    
}
      
}